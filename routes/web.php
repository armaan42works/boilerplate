<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');

})->name('home');

Route::get('/logout', [

    'uses' => 'AuthenticationController@logout',

    'as' => 'logout'

]);

Route::group([ 'middleware' => [ 'guest' ] ], function() {

    Route::group([ 'prefix' => 'register' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@register',

            'as' => 'register'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@registeration',

        ]);

    });

    Route::group([ 'prefix' => 'verify' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@verify_email',

            'as' => 'verify.email'

        ]);

        Route::get('/resend', [

            'uses' => 'AuthenticationController@resend_verify_email',

            'as' => 'verify.email.resend'

        ]);

    });

    Route::group([ 'prefix' => 'login' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@login',

            'as' => 'login'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@authenticate',

        ]);

    });

    Route::group([ 'prefix' => 'social-authentication' ], function() {

        Route::get('/{provider}', [

            'uses' => 'AuthenticationController@social_authentication',
        
            'as' => 'social.authentication'
        
        ]);
        
        Route::get('/{provider}/callback', [
        
            'uses' => 'AuthenticationController@social_authentication_callback',
        
            'as' => 'social.authentication.callback'
        
        ]);

    });

    Route::group([ 'prefix' => 'forgot-password' ], function(){

        Route::get('/', [

            'uses' => 'AuthenticationController@forgot_password',

            'as' => 'forgot.password'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@forgot_password_mail'

        ]);

    });

    Route::group([ 'prefix' => 'reset-password' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@reset_password',

            'as' => 'reset.password'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@reset_password_mail',

        ]);

    });

});

Route::group([ 'prefix' => 'admin', 'middleware' => [ 'auth' ] ], function() {

    Route::get('/dashboard', [

        'uses' => 'Admin\DashboardController@index',

        'as' => 'admin.dashboard'

    ]);

    Route::group([ 'prefix' => 'profile' ], function() {

        Route::get('/', [

            'uses' => 'Admin\ProfileController@index',

            'as' => 'admin.profile'

        ]);

        Route::post('/', [

            'uses' => 'Admin\ProfileController@update',

        ]);

        Route::post('/image', [

            'uses' => 'Admin\ProfileController@image',

            'as' => 'admin.profile.image'

        ]);

        Route::post('/password', [

            'uses' => 'Admin\ProfileController@password',

            'as' => 'admin.profile.password'

        ]);

    });

    Route::group([ 'prefix' => 'roles' ], function() {

        Route::get('/', [

            'uses' => 'Admin\RolesController@index',

            'as' => 'admin.roles'

        ]);

        Route::post('/', [

            'uses' => 'Admin\RolesController@create',

        ]);

    });

});