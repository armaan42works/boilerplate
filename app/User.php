<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'role_id', 'username', 'first_name', 'last_name', 'email', 'password',
    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    
        'password', 'remember_token',
    
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    
        'email_verified_at' => 'datetime',
    
    ];

    public function reset_password()
    {

        return $this->hasOne('App\PasswordReset', 'email', 'email');

    }

    public function role()
    {

        return $this->belongsTo('App\Role', 'role_id', 'id');

    }

    public function social_accounts()
    {
    
        return $this->hasMany('App\SocialAccount', 'user_id', 'id');
    
    }
}
