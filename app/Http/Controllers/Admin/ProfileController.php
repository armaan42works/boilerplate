<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('admin.profile.index', compact('user'));
    }

    /**
     * Update profile image.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'first-name'=> 'required'

        ]);

        if($validator->fails()) {

            Session::flash('error', 'Please enter your first name.');

            return redirect()->back();

        }

        $user = Auth::user();

        $user->first_name = $request->input('first-name');

        $user->last_name = $request->input('last-name');

        $user->gender = $request->input('gender');

        $user->save();

        Session::flash('success', 'Profile information updated successfully.');

        return redirect()->route('admin.profile');
        
    }

    /**
     * Update profile image.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function image(Request $request)
    {

        $input_validator = Validator::make($request->all(), [

            'image' => 'required'

        ]);

        if($input_validator->fails()) {

            Session::flash('error', 'Please select an image to upload.');

            return redirect()->back();

        }

        $image_validator = Validator::make($request->all(), [

            'image' => 'image'

        ]);

        if($image_validator->fails()) {

            Session::flash('error', 'Please select a valid image to upload.');

            return redirect()->back();

        }

        $format_validator = Validator::make($request->all(), [

            'image' => 'mimes:jpeg,png,jpg,gif,svg'

        ]);

        if($format_validator->fails()) {

            Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');

            return redirect()->back();

        }

        $user = Auth::user();

        if ($request->hasFile('image')) {

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

			$image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

            $user->save();

            Session::flash('success', 'Profile image updated successfully.');

            return redirect()->route('admin.profile');

        }

        abort(404);

    }

    public function password(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'old-password' => 'required',

            'new-password' => 'required',

            'confirm-new-password' => 'required'

        ]);

        if($validator->fails()) {

            if($validator->errors()->first('old-password')) {

                Session::flash('error', 'Please enter account old password.');

                return redirect()->back();

            }

            if($validator->errors()->first('new-password')) {

                Session::flash('error', 'Please enter new password.');

                return redirect()->back();

            }

            if($validator->errors()->first('confirm-new-password')) {

                Session::flash('error', 'Please enter confirm new password.');

                return redirect()->back();

            }

        }

        $password_validator = Validator::make($request->all(), [

            'confirm-new-password' => 'same:new-password'

        ]);

        if($password_validator->fails()) {

            Session::flash('error', 'New password and Confirm new password do not match.');

            return redirect()->back();

        }

        $user = Auth::user();

        if(!Hash::check($request->input('old-password'), $user->password)) {

            Session::flash('error', 'Old password do not match account password.');

            return redirect()->back();

        }

        $user->password = Hash::make($request->input('new-password'));

        $user->save();

        Session::flash('success', 'Account password updated sucessfully.');

        return redirect()->back();

    }

}
