<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::check()) {

            $user = Auth::user();

            if($user->role->slug == 'administrator') {

                return redirect()->route('admin.dashboard');
                
            } elseif($user->role->slug == 'regular') {

                return redirect()->route('regular.dashboard');

            }
        
        }

        return $next($request);
    
    }

}
