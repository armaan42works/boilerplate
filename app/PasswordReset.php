<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'email', 'token'
    
    ];

    /**
     * The name of the "updated_at" column.
     *
     * @var string
     */
    const UPDATED_AT = null;

    public function user()
    {

        return $this->belongsTo('App\User', 'email', 'email');

    }

}
