@extends('auth.layouts.app')

@section('content')

<div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            
            <div class="card-body p-0">
            
                <div class="row">
                
                    <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                
                    <div class="col-lg-6">
                
                        <div class="p-5">
                    
                            <div class="text-center">
                    
                                <h1 class="h4 text-gray-900 mb-2">Reset Password</h1>
                    
                            </div>

                            <hr>
                    
                            <form class="user" method="POST" action="{{ route('reset.password') }}">

                                @csrf

                                <input type="hidden" name="token" value="{{ $token->token }}">
                    
                                <div class="form-group">
                        
                                    <input type="email" name="email" class="form-control form-control-user" placeholder="Enter Email Address">
                    
                                </div>

                                <div class="form-group">
                        
                                    <input type="password" name="password" class="form-control form-control-user" placeholder="New Password">
                    
                                </div>

                                <div class="form-group">
                        
                                    <input type="password" name="confirm-password" class="form-control form-control-user" placeholder="Confirm New Password">
                    
                                </div>
                    
                                <button type="submit" class="btn btn-primary btn-user btn-block">Reset Password</button>
                    
                            </form>
                    
                            <hr>
                    
                            <div class="text-center">
                    
                                <a class="small" href="{{ route('register') }}">Create an Account!</a>
                    
                            </div>
                    
                            <div class="text-center">
                    
                                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
                    
                            </div>
                
                        </div>
                
                    </div>
            
                </div>
            
            </div>
        
        </div>

    </div>

</div>

@endsection