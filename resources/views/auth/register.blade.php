@extends('auth.layouts.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
      
    <div class="card-body p-0">
    
        <div class="row">
        
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          
            <div class="col-lg-7">
            
                <div class="p-5">
              
                    <div class="text-center">
                
                        <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              
                    </div>

                    @if(Session::has('success'))

                        <div class="card" style="border-radius: 500px; margin-bottom: 15px; border-color: green; color:green;">

                            <div class="card-body">
                            
                                <p style="margin: 0;">{{ Session::get('success') }}</p>

                            </div>

                        </div>

                    @endif

                    @if($errors->any())

                        <div class="card" style="border-radius: 500px; margin-bottom: 15px; border-color: red">

                            <div class="card-body">
                            
                                <ul style="list-style-type:none; margin: 0; color:red">

                                    @foreach($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        </div>

                    @endif
              
                    <form class="user" method="POST" action="{{ route('register') }}">

                        @csrf
              
                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                    
                                <input type="text" class="form-control form-control-user" name="first-name" placeholder="First Name">

                            </div>
                  
                            <div class="col-sm-6">
                  
                                <input type="text" class="form-control form-control-user" name="last-name" placeholder="Last Name">
                  
                            </div>
                    
                        </div>

                        <div class="form-group">
                    
                            <input type="text" class="form-control form-control-user" name="username" placeholder="Username">
                
                        </div>
                
                        <div class="form-group">
                    
                            <input type="email" class="form-control form-control-user" name="email" placeholder="Email Address">
                
                        </div>
                
                        <div class="form-group row">
                
                            <div class="col-sm-6 mb-3 mb-sm-0">
                    
                                <input type="password" class="form-control form-control-user" name="password" placeholder="Password">
                  
                            </div>
                  
                            <div class="col-sm-6">
                  
                                <input type="password" class="form-control form-control-user" name="password-confirmation" placeholder="Confirm Password">
                  
                            </div>
                
                        </div>
                
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            
                            Register
                        
                        </button>
                
                        <hr>

                        <div class="row">

                            <div class="col-sm-6 mb-3 mb-sm-0">

                                <a href="{{ route('social.authentication', [ 'provider' => 'google' ]) }}" class="btn btn-google btn-user btn-block">
                        
                                    <i class="fab fa-google fa-fw"></i> Register with Google
                        
                                </a>

                            </div>

                            <div class="col-sm-6">

                                <a href="{{ route('social.authentication', [ 'provider' => 'facebook' ]) }}" class="btn btn-facebook btn-user btn-block">
                        
                                    <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                        
                                </a>

                            </div>

                        </div>
              
                    </form>
        
                    <hr>
              
                    <div class="text-center">
                
                        <a class="small" href="{{ route('forgot.password') }}">Forgot Password?</a>
              
                    </div>
              
                    <div class="text-center">
                    
                        <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
              
                    </div>
            
                </div>
          
            </div>
        
        </div>
      
    </div>

</div>

@endsection
