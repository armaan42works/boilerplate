@extends('admin.layouts.app')

@section('title', 'Roles')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Roles</h1>
    
</div>

<div class="row">

    <div class="col-lg-4">

        <div class="card shadow mb-4">
        
            <div class="card-header py-3">
            
                <h6 class="m-0 font-weight-bold text-primary">New Role</h6>
            
            </div>
            
            <div class="card-body">

                <form action="{{ route('admin.roles') }}" method="POST">
            
                    @csrf

                    <div class="form-group">

                        <label>Name</label>

                        <input type="text" class="form-control" name="name" placeholder="Name" required>

                    </div>

                    <div class="form-group">

                        <label>Description</label>

                        <textarea class="form-control" name="description" placeholder="Description"></textarea>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create</span>
                  
                    </button>

                </form>
            
            </div>
            
        </div>

    </div>

    <div class="col-lg-8">

        <div class="card shadow mb-4">

            <div class="card-header text-right">
            
                <label style="margin:0">Limit:</label>

                <select name="limit">

                    <option value="10" @if(request()->query('limit') && request()->query('limit') == 10) selected @endif>10</option>

                    <option value="25" @if(request()->query('limit') && request()->query('limit') == 25) selected @endif>25</option>

                    <option value="50" @if(request()->query('limit') && request()->query('limit') == 50) selected @endif>50</option>

                    <option value="75" @if(request()->query('limit') && request()->query('limit') == 75) selected @endif>75</option>

                    <option value="100" @if(request()->query('limit') && request()->query('limit') == 100) selected @endif>100</option>

                </select>
            
            </div>
            
            <div class="card-body">

                <table class="table table-bordered table-hover">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Name</th>
        
                            <th scope="col">Description</th>
    
                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>
  
                    <tbody>

                        @if($roles->isEmpty())

                        @else

                            @foreach($roles as $role)
    
                            <tr>
            
                                <td>{{ $role->name }}</td>
        
                                <td>{{ $role->description }}</td>
        
                                <td>N/A</td>
        
                            </tr>

                            @endforeach

                        @endif
  
                    </tbody>

                </table>

                <div class="pagination-wrap">

                    <span class="entries float-right" style="margin-top: 7px">

                        Showing {{ $roles->firstItem() }} to {{ $roles->lastItem() }} of total {{ $roles->total() }} entries

                    </span>

                    @php
                    
                        $paginate = [];

                        if(request()->query('search')) {

                            $paginate['search'] = request()->query('search');

                        }

                        if(request()->query('limit')) {

                            $paginate['limit'] = request()->query('limit');

                        }

                    @endphp

                    {{ $roles->appends($paginate)->links() }}

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection