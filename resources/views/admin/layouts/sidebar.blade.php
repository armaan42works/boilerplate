<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="javascript:void();">
    
        <div class="sidebar-brand-icon rotate-n-15">
        
            <i class="fab fa-laravel"></i>
        
        </div>
        
        <div class="sidebar-brand-text mx-3">S.O.H.</div>
      
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ request()->is('admin/dashboard') ? 'active' : '' }}">
    
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
        
            <i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
        
        </a>
      
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">Manage</div>

    <li class="nav-item {{ request()->is('admin/roles') ? 'active' : '' }}">

        <a class="nav-link" href="{{ route('admin.roles') }}">
        
            <i class="fas fa-user-tag"></i> <span>Roles</span>
        
        </a>
    
    </li>

    <li class="nav-item">

        <a class="nav-link {{ !request()->is('admin/users/*') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-users" aria-expanded="true" aria-controls="collapse-users">

            <i class="fas fa-users"></i> <span>Users</span>
    
        </a>
    
        <div id="collapse-users" class="collapse" data-parent="#accordionSidebar">
    
            <div class="bg-white py-2 collapse-inner rounded">
        
                <a class="collapse-item {{ request()->is('admin/users/create') ? 'active' : '' }}" href="javascript:void();">New User</a>
        
                <a class="collapse-item {{ request()->is('admin/users') ? 'active' : '' }}" href="javascript:void();">All User</a>
        
            </div>
    
        </div>
    
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
    
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    
    </div>

</ul>