@extends('admin.layouts.app')

@section('title', 'Profile')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    
</div>

<div class="row">

    <div class="col-lg-4">

        <div class="card shadow mb-4">
        
            <div class="card-header py-3">
            
                <h6 class="m-0 font-weight-bold text-primary">Image</h6>
            
            </div>
            
            <div class="card-body text-center">

                @php
                
                    if(auth()->user()->image) {

                        $user_image = asset('uploads/images/users/' . auth()->user()->id . '/' . auth()->user()->image);

                    } else {

                        $image = 'user';

                        if(auth()->user()->gender == 'male' || auth()->user()->gender == 'female') {

                            $image .= '-' . auth()->user()->gender;

                        }

                        $image .= '.png';

                        $user_image = asset('images/elements/' . $image);

                    }
                
                @endphp

                <img src="{{ $user_image }}" alt="profile-image" id="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                <hr>

                <form action="{{ route('admin.profile.image') }}" method="POST" id="update-profile-image" enctype="multipart/form-data">
            
                    @csrf

                    <input type="file" name="image" accept="image/*" style="display:none" required>

                    <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="far fa-image"></i>
                        
                        </span>
                        
                        <span class="text">Change</span>
                    
                    </button>

                    <button type="submit" class="btn btn-success btn-icon-split" style="display: none;">

                        <span class="icon text-white-50">
                        
                            <i class="fas fa-check"></i>
                        
                        </span>
                        
                        <span class="text">Upload</span>
                    
                    </button>

                </form>
            
            </div>
            
        </div>

    </div>

    <div class="col-lg-8">

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
            
            </div>

            <div class="card-body">

                <form class="personal-information" method="POST" action="{{ route('admin.profile') }}">

                    @csrf

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>First Name</label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ $user->first_name }}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Last Name</label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ $user->last_name }}">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Username</label>

                                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ $user->username }}" disabled>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Gender</label>

                                <select class="form-control" name="gender">

                                    <option value="">Select</option>

                                    <option value="male" @if($user->gender == 'male') selected @endif>Male</option>
    
                                    <option value="female" @if($user->gender == 'female') selected @endif>Female</option>
            
                                    <option value="others" @if($user->gender == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label>Email Address</label>

                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ $user->email }}" disabled>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update</span>
    
                    </button>

                </form>

            </div>

        </div>

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
            
            </div>

            <div class="card-body">

                <form class="change-password" method="POST" action="{{ route('admin.profile.password') }}">

                    @csrf

                    <div class="form-group">

                        <label>Old Password</label>

                        <input type="password" class="form-control" name="old-password" placeholder="Old Password" autocomplete="off" required>

                    </div>

                    <div class="form-group">

                        <label>New Password</label>

                        <input type="password" class="form-control" name="new-password" placeholder="New Password" autocomplete="off" required>

                    </div>

                    <div class="form-group">

                        <label>Confirm New Password</label>

                        <input type="password" class="form-control" name="confirm-new-password" placeholder="Confirm New Password" autocomplete="off" required>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Change Password</span>
    
                    </button>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {

    $('#change-profile-image').on('click', function(event) {

        $('#update-profile-image input[name="image"]').click();

        event.preventDefault();

    });

    $('#update-profile-image input[name="image"]').on('change', function(event) {

        previewImage(this, '#profile-image');

        $('#update-profile-image button[type="submit"]').show();

    });

})

</script>

@endsection