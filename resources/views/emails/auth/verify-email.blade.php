@component('mail::message')
# Hello {{ $user->first_name }},
<br>
Your account is successfully created. In order to use your account, you need to activate the account by clicking the link below.

@component('mail::button', [ 'url' => route('verify.email', [ 'username' => $user->username, 'token' => $user->password ]) ])
Click Here
@endcomponent

Regards,<br>
{{ env('APP_NAME') }}
@endcomponent
