@component('mail::message')
# Hello {{ $reset->user->first_name }},
<br>
We have recieved a request to reset account password. Click on the link below to change your account password.

@component('mail::button', [ 'url' => route('reset.password', [ 'token' => $reset->token ]) ])
Click Here
@endcomponent

If you haven't requested for password reset, Kindly ignore this email.

Regards,<br>
{{ env('APP_NAME') }}
@endcomponent
