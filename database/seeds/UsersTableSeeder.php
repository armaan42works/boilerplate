<?php

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        
        $roles = Role::all();

        foreach($roles as $role) {
        
            $user = User::create([
            
                'role_id' => $role->id,
            
                'username' => $role->slug,
            
                'first_name' => $role->name,
            
                'email' => $role->slug . '@showofhands.com',
            
                'password' => Hash::make('demodemo')
            
            ]);
            
            $user->email_verified_at = Carbon::now();
            
            $user->save();

        }

    }
}
