<?php

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [

            'administrator', 'moderator', 'regular'

        ];

        foreach($roles as $role) {

            Role::create([
                
                'slug' => Str::slug($role, '-'),

                'name' => Str::title($role),

                'description' => 'User with ' . strtolower($role) . ' role.'

            ]);

        }
    }
}
